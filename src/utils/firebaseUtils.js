import firebase from 'firebase';

const config = {
   

    apiKey: "AIzaSyAh6F4ITgkeCMYbGmcBdhdRtsTzQZ5nbcw",
    authDomain: "desafio-onovolab-f6366.firebaseapp.com",
    databaseURL: "https://desafio-onovolab-f6366.firebaseio.com",
    projectId: "desafio-onovolab-f6366",
    storageBucket: "",
    messagingSenderId: "457480740248",
    // appId: "1:457480740248:web:325cad70e7c646e5729fed",
    // measurementId: "G-7YY0CDFVG1"
};

export const firebaseImpl = firebase.initializeApp(config);
export const firestore = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings); 